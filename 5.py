import numpy as np


def task_1(m1, n1):
    """
    Функция создания двумерного массива, где a[i][j] = i + 2 * j

    Принимет на вход высоту и ширину массива. Создает по ним массив, где
    каждый элемент равен a[i][j] = i + 2 * j.
    Функция ничего не возвращает, но печатает массив.

    :param m1: высота массива
    :param n1: ширина массива

    :return: -

    :raises:
    TypeError

    :examples:
     > task_1(2,2)
    [[0 2]
    [1 3]]

    > task_1(2)
    Traceback (most recent call last):
    ...
    TypeError: task_1() missing 1 required positional argument: 'n1'

    > task_1('efrelg','fkwe')
    Traceback (most recent call last):
    ...
    TypeError: 'str' object cannot be interpreted as an integer
    """
    a = np.zeros((m1, n1), dtype='int')
    for i in range(m1):
        for j in range(n1):
            a[i][j] = i + 2 * j
    print(a)


def task_2(a):
    """
    Функция вывода главной и побочной диагонали

    Функция принимает на вход двумерный массив и
    выводит на экран главную и побочную диагональ.
    Она ничего не возвращает, но печатает диагонали.

    :param a: двумерный массив

    :return: -

    :raises:
    TypeError

    :examples:
    > arr = [[ 9 63 49]
        [54 61 46]
        [63 26 35]]
    > task_2(arr)
    > Главная диагональ [ 9 61 35]
    Побочная диагональ [49 61 63]

    > task_2('rdhj')
    Traceback (most recent call last):
    ...
    TypeError: 'str' object cannot be interpreted as an list

    > arr = [[ r 63 n]
        [54 61 46]
        [63 26 35]]
    > task_2(arr)
    > Traceback (most recent call last):
    ...
    TypeError: 'str' object cannot be interpreted as an integer
    """
    print('Главная диагональ', a.diagonal())
    print('Побочная диагональ', np.fliplr(a).diagonal())


def task_3(a, m1, n1):
    """
    Функция нахождения суммы элементов

    Функция принимает на вход двумерный массив, его высоту и ширину.
    Вычисляет сумму тех положительных элементов двумерного массива А,
    которые стоят в строках, не содержащих нулевых элементов.
    Она ничего не возвращает, но печатает сумму.

    :param a: двумерный массив
    :param m1: высота массива
    :param n1: ширина массива

    :return: -

    :raises:
    TypeError

    :examples:
    > arr = [[ 9 63 0]
        [0 61 46]
        [63 -26 35]]

    > task_3(arr,3,3)
    > 98

    > task_3(arr,2)
    Traceback (most recent call last):
    ...
    TypeError: task_3() missing 1 required positional argument: 'n1'
    > task_1('efrelg',4,1)
    Traceback (most recent call last):
    ...
    TypeError: 'str' object cannot be interpreted as an list

    > arr = [[ r 63 n]
        [54 61 46]
        [63 26 35]]
    > task_3(arr,3,3)
    > Traceback (most recent call last):
    ...
    TypeError: 'str' object cannot be interpreted as an integer
    """
    b = [False] * m1
    for i in range(m1):
        for j in range(n1):
            if a[i][j] == 0:
                b[i] = True
    summa = 0
    for i in range(m1):
        if b[i] is False:
            for j in range(n1):
                if a[i][j] > 0:
                    summa += a[i][j]
    print(summa)


def main():
    while True:
        print('Выберите способ задания массива:'
              '\n1 - Автоматически'
              '\n2 - Вручную'
              '\n3 - Закончить программу'
              )
        crit = input('Введите критерий: ')
        if crit == '1':
            m, n = map(int, input('Введите высоту и ширину: ').split())
            start, end = map(int, input('Введите диапозон: ').split())
            arr = np.random.randint(start, end, (m, n))
            print(arr)
        elif crit == '2':
            m, n = map(int, input('Введите высоту и ширину: ').split())
            arr = np.zeros((m, n), dtype='int')
            for i in range(m):
                for j in range(n):
                    arr[i][j] = input()
            print(arr)
        elif crit == '3':
            break
        while True:
            print('Привет')
            print('Выберите задание:'
                  '\n1 - 1.13 Получить массив, для которого aij=i+2j.'
                  '\n2 - 2.7 Напечатать левую и правую диагонали двумерного массива'
                  '\n3 - 3.34 Составьте программу вычисления суммы тех положительных элементов двумерного массива А, '
                  'которые стоят в строках, не содержащих нулевых элементов. '
                  '\n4 - Возврат назад'
                  )
            crit_1 = input('Выберите задание: ')
            if crit_1 == '1':
                task_1(m, n)
            elif crit_1 == '2':
                task_2(arr)
            elif crit_1 == '3':
                task_3(arr, m, n)
            elif crit_1 == '4':
                break


if __name__ == '__main__':
    main()